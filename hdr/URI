; Copyright 2000 Pace Micro Technology plc
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;
; File          : URI.s
; Purpose       : Assembler interface to URI handler
; Author        : Ben Laughton
; History       : 17/04/1997

        SUBT    Exported URI handler constants

OldOpt  SETA    {OPT}
        OPT     OptNoList+OptNoP1List

; ***********************************
; ***    C h a n g e   L i s t    ***
; ***********************************

; Date       Name  Description
; ----       ----  -----------
; 17-Apr-97  BAL   Created
; 14-Apr-00  ADH   Updated to follow "correct" style based on FileCore;
;                       assumes Global.SWIs has URISWI_Base etc. defined

SWIClass        SETS    URISWI_Name

        ^       URISWI_Base

        AddSWI  Version
        AddSWI  Dispatch
        AddSWI  RequestURI
        AddSWI  InvalidateURI

        OPT     OldOpt
        END
